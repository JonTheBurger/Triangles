#ifndef NONCOPYABLE_HXX
#define NONCOPYABLE_HXX

class NonCopyable {
protected:
  NonCopyable() = default;
  ~NonCopyable() = default;

public:
  NonCopyable(const NonCopyable&) = delete;
  void operator=(const NonCopyable&) = delete;
};

class NonMovable {
protected:
  NonMovable() = default;
  ~NonMovable() = default;

public:
  NonMovable(NonMovable&&) = delete;
  void operator=(NonMovable&&) = delete;
};

#endif // NONCOPYABLE_HXX
