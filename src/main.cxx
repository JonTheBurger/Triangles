/*
  .vert - a vertex shader
  .tesc - a tessellation control shader
  .tese - a tessellation evaluation shader
  .geom - a geometry shader
  .frag - a fragment shader
  .comp - a compute shader

  https://openal.org/documentation/
*/

#include <fstream>
#include <streambuf>
#include <string>
#include <vector>

// clang-format off - glad.h must be included before any OpenGL headers
#include <glad/glad.h>
// clang-format on
#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alut.h>
#include <GLFW/glfw3.h>
//#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

#include "Endian.hxx"

void FrameBufferCallback(GLFWwindow* window, int width, int height);
void ProcessInput(GLFWwindow* window);
void AlLogError(spdlog::logger& log, const char* file = "", int line = 0);
void AlutLogError(spdlog::logger& log, const char* file = "", int line = 0);

#define AL_LOG_ERROR(log) AlLogError(log, __FILE__, __LINE__)
#define ALUT_LOG_ERROR(log) AlutLogError(log, __FILE__, __LINE__)

int main(int argc, char* argv[]) {
  auto log = spdlog::stdout_color_mt("console");
  log->set_level(spdlog::level::trace);
  log->flush_on(spdlog::level::trace);
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#if __APPLE__
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

  GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", nullptr, nullptr);
  if (window == nullptr) {
    log->critical("Failed to create GLFW window");
    glfwTerminate();
    return -1;
  }
  glfwMakeContextCurrent(window);
  glfwSetFramebufferSizeCallback(window, FrameBufferCallback);

  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    log->critical("Failed to initialize GLAD");
    return -1;
  }
  glViewport(0, 0, 800, 600);

  float vertices[] = {
    0.5f,  0.5f,  0.0f, // top right
    0.5f,  -0.5f, 0.0f, // bottom right
    -0.5f, -0.5f, 0.0f, // bottom left
    -0.5f, 0.5f,  0.0f  // top left
  };
  unsigned int indices[] = {
    0, 1, 3, // first triangle
    1, 2, 3  // second triangle
  };

  unsigned vbo;
  glGenBuffers(1, &vbo);

  // Copy vertex data
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices[0], GL_STATIC_DRAW);
  // set vertex attributes; values from res/basic.vert
  static constexpr auto VertexLayoutLocation = 0;
  static constexpr auto SizeofVertexAttribVec3 = 3;
  glVertexAttribPointer(VertexLayoutLocation,
                        SizeofVertexAttribVec3,
                        GL_FLOAT,
                        GL_FALSE,
                        SizeofVertexAttribVec3 * sizeof(float),
                        nullptr);
  glEnableVertexAttribArray(0);

  int success;
  char infoLog[512];

  std::ifstream inVert("data/basic.vert");
  std::string vertStr((std::istreambuf_iterator<char>(inVert)), std::istreambuf_iterator<char>());
  const char* const VertexShaderSource = vertStr.c_str();
  unsigned vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &VertexShaderSource, nullptr);
  glCompileShader(vertexShader);
  glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(vertexShader, sizeof(infoLog), nullptr, &infoLog[0]);
    log->error("SHADER::VERTEX::COMPILATION_FAILED\n {}", infoLog);
  }

  std::ifstream inFrag("data/basic.frag");
  std::string fragStr((std::istreambuf_iterator<char>(inFrag)), std::istreambuf_iterator<char>());
  const char* const FragmentShaderSource = fragStr.c_str();
  unsigned fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glad_glShaderSource(fragmentShader, 1, &FragmentShaderSource, nullptr);
  glad_glCompileShader(fragmentShader);
  glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(fragmentShader, sizeof(infoLog), nullptr, &infoLog[0]);
    log->error("SHADER::FRAGMENT::COMPILATION_FAILED\n {}", infoLog);
  }

  unsigned shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);
  glLinkProgram(shaderProgram);
  glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
  if (!success) {
    glGetProgramInfoLog(shaderProgram, sizeof(infoLog), nullptr, &infoLog[0]);
    log->error("SHADER::PROGRAM::LINKING_FAILED\n {}", infoLog);
  }
  glUseProgram(shaderProgram);
  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);

  unsigned ebo;
  glGenBuffers(1, &ebo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

  unsigned vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
  glEnableVertexAttribArray(0);

  //  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  // Load Audio
  std::vector<std::string> audioDevices;
  if (alcIsExtensionPresent(nullptr, "ALC_ENUMERATION_EXT")) {
    const char* audioDeviceName = nullptr;
    log->info("Audio Device Discovery:");
    for (audioDeviceName = alcGetString(nullptr, ALC_DEVICE_SPECIFIER);
         audioDeviceName != nullptr && audioDeviceName[0] != '\0';
         audioDeviceName += strlen(audioDeviceName) + sizeof(static_cast<char>('\0'))) {
      audioDevices.emplace_back(audioDeviceName);
      log->info("- {}", audioDevices.back());
    }
  } else {
    log->warn("AUDIO::EXTENSION::QUERY_NOT_SUPPORTED");
  }
  if (audioDevices.size() == 0) { log->error("AUDIO::DEVICE::NONE_FOUND"); }

  alutInit(&argc, argv);
  ALUT_LOG_ERROR(*log);

  const float listenerOrientation[] = {
    0.f, 0.f, -1.f, // "at" vector, forward pointing perspective of listener
    0.f, 1.f, 0.f,  // "up" vector, upwards direction from perspective of listener, tangent to at
  };
  alListener3f(AL_POSITION, 0.f, 0.f, 1.f);
  AL_LOG_ERROR(*log);
  alListener3f(AL_VELOCITY, 0.f, 0.f, 0.f);
  AL_LOG_ERROR(*log);
  alListenerfv(AL_ORIENTATION, listenerOrientation);
  AL_LOG_ERROR(*log);

  unsigned audioSource;
  alGenSources(1, &audioSource);
  AL_LOG_ERROR(*log);
  alSourcef(audioSource, AL_PITCH, 1);
  AL_LOG_ERROR(*log);
  alSourcef(audioSource, AL_GAIN, 1);
  AL_LOG_ERROR(*log);
  alSource3f(audioSource, AL_POSITION, 0.f, 0.f, 0.f);
  AL_LOG_ERROR(*log);
  alSource3f(audioSource, AL_VELOCITY, 0.f, 0.f, 0.f);
  AL_LOG_ERROR(*log);
  alSourcei(audioSource, AL_LOOPING, AL_FALSE);
  AL_LOG_ERROR(*log);

  unsigned audioBuffer = alutCreateBufferFromFile("data/Song.wav");
  AL_LOG_ERROR(*log);
  ALUT_LOG_ERROR(*log);
  if (audioBuffer == AL_NONE) { log->error("AUDIO::BUFFER::INVALID"); }
  alSourcei(audioSource, AL_BUFFER, audioBuffer);
  AL_LOG_ERROR(*log);
  alSourcePlay(audioSource);
  AL_LOG_ERROR(*log);

  while (!glfwWindowShouldClose(window)) {
    ProcessInput(window);

    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // use vertex shader
    glUseProgram(shaderProgram);
    glBindVertexArray(vao);
    static void* const EboOffset = nullptr;
    glDrawElements(GL_TRIANGLES, sizeof(indices) / sizeof(*indices), GL_UNSIGNED_INT, EboOffset);

    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  alDeleteSources(1, &audioSource);
  alDeleteBuffers(1, &audioBuffer);
  alutExit();

  glDeleteVertexArrays(1, &vao);
  glDeleteBuffers(1, &vbo);
  glDeleteBuffers(1, &ebo);
  glfwTerminate();

  log->info("Exit Success");
  return 0;
}

void FrameBufferCallback(GLFWwindow* window, int width, int height) {
  (void)window;
  glViewport(0, 0, width, height);
}

void ProcessInput(GLFWwindow* window) {
  if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) { glfwSetWindowShouldClose(window, true); }
}

void AlLogError(spdlog::logger& log, const char* const file, const int line) {
  const ALenum err = alGetError();
  const char* str = alGetString(err);
  str = (str != nullptr) ? str : "AL Unknown Error";
  if (err != AL_NO_ERROR) { log.error("{}\t{}:{}", str, file, line); }
}

void AlutLogError(spdlog::logger& log, const char* const file, const int line) {
  const ALenum err = alutGetError();
  const char* str = alutGetErrorString(err);
  str = (str != nullptr) ? str : "ALUT Unknown Error";
  if (err != AL_NO_ERROR) { log.error("{}\t{}:{}", str, file, line); }
}
