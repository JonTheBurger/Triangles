#ifndef ENDIAN_HXX
#define ENDIAN_HXX

#include <algorithm>
#include <type_traits>

enum class endian {
#ifdef _WIN32
  little = 0,
  big = 1,
  native = little
#else
  little = __ORDER_LITTLE_ENDIAN__,
  big = __ORDER_BIG_ENDIAN__,
  native = __BYTE_ORDER__
#endif
};

template<typename T>
std::enable_if_t<std::is_integral<T>::value && endian::native == endian::big, T> EndianFixLittle(
    T t) {
  std::reverse(reinterpret_cast<char*>(&t), reinterpret_cast<char*>(&t + 1));
  return t;
}

template<typename T>
std::enable_if_t<std::is_integral<T>::value && endian::native == endian::little, T> EndianFixLittle(
    T t) {
  return t;
}

template<typename T>
std::enable_if_t<std::is_integral<T>::value && endian::native == endian::big, T> EndianFixBig(T t) {
  return t;
}

template<typename T>
std::enable_if_t<std::is_integral<T>::value && endian::native == endian::little, T> EndianFixBig(
    T t) {
  std::reverse(reinterpret_cast<char*>(&t), reinterpret_cast<char*>(&t + 1));
  return t;
}

#endif // ENDIAN_HXX
