#!/bin/sh
# OpenGL
sudo apt-get install -y mesa-common-dev
sudo apt-get install -y libglu1-mesa-dev
# glfw3
sudo apt-get install -y libxinerama-dev
sudo apt-get install -y libxcursor-dev
sudo apt-get install -y libglfw3-dev
# openal
sudo apt-get install -y libopenal-dev
sudo apt-get install -y libalut-dev
# gtest
sudo apt-get install -y google-mock
# spdlog
sudo apt-get install -y libspdlog-dev

