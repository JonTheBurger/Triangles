function(copy_dlls_to_build EXE)
    if("${CMAKE_CXX_COMPILER_ID}" MATCHES "MSVC")
        get_property(DLLS TARGET ${EXE} PROPERTY LINK_LIBRARIES)
        foreach(DLL ${DLLS})
            if(TARGET ${DLL})
                get_target_property(TARGET_TYPE ${DLL} TYPE)
                if (TARGET_TYPE MATCHES "(SHARED_LIBRARY|MODULE_LIBRARY)")
                    add_custom_command(TARGET ${EXE} POST_BUILD
                        COMMAND ${CMAKE_COMMAND} -E copy_if_different
                        $<TARGET_FILE:${DLL}>
                        $<TARGET_FILE_DIR:${EXE}>
                    )
                endif()
            endif()
        endforeach()
    endif()
endfunction()
